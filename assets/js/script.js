let url = "http://data.metromobilite.fr/api/routers/default/index/routes";
$.ajax({
    url: url,
    type: "GET", // par défaut
    dataType: "json", // ou HTML par ex

    // Si succès 
    success: function (data, statut) {
        console.log(data);
        for (let i = 0; i < data.length; i++) {
            let nom = data[i].shortName;
            let p = $(`<p>${nom}</p>`);
            p.click(data[i], click);
            if (data[i].type == 'TRAM') {
                $("#lTram").append(p);
                p.css({
                    backgroundColor: '#' + data[i].color,
                    border: "solid 1px" + data[i].color,
                    color: "#" + data[i].textColor,
                    width: "20px",
                    height: "20px",
                    borderRadius: "50%",
                    textAlign: "center",
                    display: "inline-block"
                });
            }
            if (data[i].type == 'CHRONO') {
                $("#lChrono").append(p);
                p.css({
                    backgroundColor: '#' + data[i].color,
                    border: "solid 1px" + data[i].color,
                    color: "#" + data[i].textColor,
                    width: "20px",
                    height: "20px",
                    borderRadius: "25%",
                    textAlign: "center",
                    display: "inline-block"
                });
            }
            if (data[i].type == 'PROXIMO') {
                $("#lProximo").append(p);
                p.css({
                    backgroundColor: '#' + data[i].color,
                    border: "solid 1px" + data[i].color,
                    color: "#" + data[i].textColor,
                    width: "20px",
                    height: "20px",
                    borderRadius: "25%",
                    textAlign: "center",
                    display: "inline-block"
                });
            }
            if (data[i].type == 'FLEXO') {
                $("#lFlexo").append(p);
                p.css({
                    backgroundColor: '#' + data[i].color,
                    border: "solid 1px" + data[i].color,
                    color: "#" + data[i].textColor,
                    width: "20px",
                    height: "20px",
                    borderRadius: "25%",
                    textAlign: "center",
                    display: "inline-block"
                });
            }
            if (data[i].type == 'C38') {
                $("#lC38").append(p);
                p.css({
                    backgroundColor: '#' + data[i].color,
                    border: "solid 1px" + data[i].color,
                    color: "#" + data[i].textColor,
                    width: "40px",
                    height: "20px",
                    borderRadius: "10%",
                    textAlign: "center",
                    display: "inline-block"
                });
            }

        }
        $("#lignes").accordion({
            heightStyle: "content"
        });
    },

    // Si erreur
    error: function (result, statut, erreur) {
        console.warn("ERREUR");
        console.log(result);
        console.log("Erreur : " + statut + ' ' + erreur);
    },

    // A la fin !
    complete: function (result, statut) {
        console.log(result);
        console.log("Complet : " + statut);
    }


});
let map = L.map('mapid').setView([45.188516, 5.724468], 12);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);
const usedLines = {};

function click(e) {
    console.log(e);
    let urlmap = `http://data.metromobilite.fr/api/lines/json?types=ligne&codes=${e.data.id}`;
    $.ajax({
        url: urlmap,
        type: "GET",
        dataType: "json",

        success: function (data, statut) {
            console.log(data);
            const geo = L.geoJSON(data, {
                style: {
                    color: "#" + e.data.color
                }
            });

            if (!usedLines[e.data.id]) {
                usedLines[e.data.id] = geo;
                usedLines[e.data.id].addTo(map);
            } else {
                usedLines[e.data.id].removeFrom(map);
                usedLines[e.data.id] = undefined;
            }

            horaires(e);
        }
    })
}

function horaires(e) {
    let urlHoraire = `http://data.metromobilite.fr/api/ficheHoraires/json?route=${e.data.id}`;
    $.ajax({
        url: urlHoraire,
        type: "GET",
        dataType: "JSON",

        success: function (data, statut) {
            console.log(data);
        }
    })
}